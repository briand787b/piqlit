package obj

// Encoding determines how to read the object
type Encoding string

const (
	// GIF is ...
	GIF Encoding = "gif"
)
